<?php
session_start();

$logged_in = false;	
$tutor_id = 0;

if (isset($_SESSION['tutor_id'])) {
	# the user is logged-in;
	$tutor_id = intval($_SESSION['tutor_id']);
	$logged_in = true;
	$login_logout = "<a href=logout.php>Logout</a>";
} else {
	$login_logout = "<a href=login.php>Login</a>";
}

/* note: to take a request out of the queue, hit this URL with that table name:  
http://dev0.library.wwu.edu/sms/update.php?table=a2
*/

require("./db-connection.php");
require("./functions.php");
require("./tables-positions.php");
require("./subject-crn.php");
require("./report-functions.php");

$_GLOBALS['tab'] = "Queue";
include("header.php");

$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

$valid_subjects_crns = get_subjects_crns($pdo);
$subject_crn_form = implode("<option>", $valid_subjects_crns);

$tables_positions = get_tables_positions($pdo);
$tables_positions_form = implode("<option>", $tables_positions);

$avg_wait_time_pretty = avg_wait($pdo);
$avg_help_time_pretty = get_help_times($pdo);

$avg_wait_per_day = avg_wait_per_day($pdo);
foreach ($avg_wait_per_day as $key=>$value) {
	$course_colors = getColors(2);
	$avg_wait_colors[] = "rgba(" . $course_colors[0] . ")";
	$avg_help_colors[] = "rgba(" . $course_colors[1] . ")";
	$wait_date[] = $key;
	$wait_time[] = $value;
}

$avg_help_per_day = avg_help_per_day($pdo);
foreach ($avg_help_per_day as $key=>$value) {
	$help_date[] = $key;
	$help_time[] = $value;
}

$avg_wait_per_course = avg_wait_per_course($pdo);
foreach ($avg_wait_per_course as $key=>$value) {
	$course_color = getColor();
	$course_wait_colors[] = "rgba(" . $course_color . ")";
	$avg_wait_per_course_courseCodes[] = $key;
	$avg_wait_per_course_waitTimes[] = $value;
}

$avg_help_per_course = avg_help_per_course($pdo);
foreach ($avg_help_per_course as $key=>$value) {
	$course_color = getColor();
	$course_help_colors[] = "rgba(" . $course_color . ")";
	$avg_help_per_course_code[] = $key;
	$avg_help_per_course_help[] = $value;
}


$questions_per_course = questions_per_course($pdo);
foreach ($questions_per_course as $key=>$value) {
	$color = getColor();
	$qpc_colors[] = "rgba(" . $color . ")";
	$qpc_courseName[] = $key;
	$qbc_count[] = $value;
}





$seats_in_use = [];

try {
	// get a list of active requests from today (ignore anything created prior to today);
	$stmt = $pdo->prepare('SELECT `id`, `status`, `table_position`, `nsrid`, `created`, `subject_crn`,  subject_crn.subject as  `subject`, subject_crn.course as `course` FROM sms_requests  LEFT JOIN subject_crn ON subject_crn.record_id = sms_requests.subject_crn WHERE status < 3  AND DATE(`created`) = DATE(NOW()) ORDER BY `created` ');
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$row_count = $stmt->rowCount();

	$now = date('g:i:s a');

	if($rows !== false) {

		// $output .= '<div>People in queue: '. $row_count. '</div>';
		$output .= "<div class='grid'>";
		$output .= "	<div class='grid-cell'><h4>Table / Seat</h4></div>";
		$output .= "	<div class='grid-cell'><h4>Waiting</h4></div>";
		$output .= "	<div class='grid-cell'> <h4>Subject / Course </h4> </div>";
		$output .= "</div>";

		// Parse the result set
		foreach($rows as $row) {
			$pretty_date = time_elapsed_string($row['created']);
			$id = $row['id'];
			$status = $row['status'];			
			if ($status == 1) {
				$status_text = $pretty_date;
			}
			if ($status == 2) {
				$status_text = " Being served ";
			}

			$nsrid = $row['nsrid'];
			$output .= "<div class='grid row'>";
			$seats_in_use[] = $row['table_position'];
			$subject_courses[] = $row["subject"] . " " . $row["course"];

	            		$theCheckbox = '<input type="radio" class="radio" required tabindex="0" name="nsrid" id="row_' . $nsrid . '" value="' . $nsrid . '"><label for="row_' . $nsrid . '" role="radio" aria-checked="false" tabindex="0" title="claim this request"></label>';

	            		$output .= "	<div class='grid-cell'>" . $theCheckbox . " &nbsp;  &nbsp;  &nbsp; " . $row['table_position'] . "</div>";
			$output .= "	<div class='grid-cell'>" . $status_text . "</div>";
			$output .= "	<div class='grid-cell'> " . $row["subject"] . " " . $row["course"] . "</div>";
			$output .= "</div>";

		}
	}

	$pdo = null;

} catch(PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}

 
 $seats_in_use_js = json_encode($seats_in_use);

$course_count = array_count_values($subject_courses);
foreach ($course_count as $key=>$value) {
	$course_numbers[] = $key;
	$num_requests[] = $value;
	$active_request_colors[] = "rgba(" . getColor()  . ")";
}

?>
		<div class="grid">
			<div class="grid-cell3">
				Text your table number (e.g., "1A", "4C") to 360-205-1081 or use the form below to be added to this list
			</div>
			<div class="grid-cell">
				Average Wait: <?php echo $avg_wait_time_pretty; ?>
			</div>
			<div class="grid-cell">
				Average Session: <?php echo $avg_help_time_pretty; ?>
			</div>
		 </div>
	       	<div class="grid">
	       		<div class="grid-cell">
				 <form method="post" action="add-to-queue.php">
					<fieldset>
						<legend>Add to queue</legend>
						<label for="seat">Seat</label>
						<select required name="seat" id="seat"><option></option><?php echo $tables_positions_form; ?></select>
						<label for="course">Course</label>
						<select required name="course" id="course"><option></option><?php echo $subject_crn_form; ?></select>
						<input type='submit' name='submit' value='Submit'>
					</fieldset>					
				</form>

<?php

	$claim_close = '';
	$current_wait_chart = '';

	if ($row_count > 0) {		

		if ($tutor_id > 0) {
			$claim_close = '
				 <form method="post" action="update.php">
			       	' . $output . '
					<div>
						<input type="submit" name="claim" value="Claim this request">
						&nbsp; &nbsp; 
						<input type="submit" name="unclaim" value="Unclaim this request">
						&nbsp; &nbsp; 
						<input type="submit" name="close" value="Close this request">
					</div>
				</form>
			';
		} else {
			$claim_close = $output;		
		}

		$current_wait_chart = '
			<canvas id="barChart" width="500" height="100"></canvas>
		';

	}

?>

				<?php echo $claim_close; ?>

			</div>
			<div class="grid-cell2">
				<?php echo $current_wait_chart; ?>
	
				<canvas id="barChart_avgwait" width="500" height="100"></canvas>

				<canvas id="barChart_wait_per_course" width="500" height="100"></canvas>

				<canvas id="barChart_help_per_course" width="500" height="100"></canvas>

				<canvas id="barChart_questions_per_course" width="500" height="100"></canvas>


			</div>
		</div>


<script>
	var seats_in_use = <?php echo $seats_in_use_js; ?>;
	var exists = false;
	$('#seat option').each(function(){
		var this_value = $(this).val();
		var position = seats_in_use.indexOf(this_value);
		// console.log(this_value, position);
		if (position !== -1) {
			// this seat is in use, disable it from the select list
			$(this).css("text-decoration", "line-through").prop("disabled", true);
			// console.log("disable", this_value);
		}
	});

	// TODO prevent someone from claiming anything but the first of it's kind (course number).
	// for instance, if there are three PHYS 104 requests in the queue, allow the tutor to only select the oldest one (not one of the newer ones)



	// chart.js
	if (document.getElementById("barChart")) {
		var bar = document.getElementById("barChart");
		var myBarChart = new Chart(bar, {
		    type: 'bar',
		    data: {
		        labels: ['<?php echo implode("','", $course_numbers); ?>'],
		        datasets: [{
		            label: '# of Active Requests',
		            data: [<?php echo implode(",", $num_requests); ?>],
		            backgroundColor: [ '<?php echo implode("','", $active_request_colors); ?>' ],
		            borderWidth: 0
		        }]
		    },
		    options: {
		    	animation: false
			, scales: {
			    yAxes: [{
			        ticks: {
			            beginAtZero:true
			        }
			    }]
			}
		    }
		});
	}


	var bar = document.getElementById("barChart_avgwait");
	var myBarChart = new Chart(bar, {
	    type: 'bar',
	    data: {
	        labels: ['<?php echo implode("','", $wait_date); ?>'],
	        datasets: [
	        	{
		            label: 'Average Wait Time (minutes)',
		            data: [<?php echo implode(",", $wait_time); ?>],
		            backgroundColor: [ '<?php echo implode("','", $avg_wait_colors); ?>' ],
		            borderColor: '#333',
		            borderWidth: 0
		}, 
	        	{
		            label: 'Average Session Time (minutes)',
		            data: [<?php echo implode(",", $help_time); ?>],
		            backgroundColor: [ '<?php echo implode("','", $avg_help_colors); ?>' ],
		            borderColor: '#333',
		            borderWidth: 1
	        	},

	        ]
	    },
	    options: {
	    	animation: false
		, scales: {
		    yAxes: [{
		        ticks: {
		            beginAtZero:true
		        }
		    }]
		}
	    }
	});



	var bar = document.getElementById("barChart_wait_per_course");
	var myBarChart = new Chart(bar, {
	    type: 'bar',
	    data: {
	        labels: ['<?php echo implode("','", $avg_wait_per_course_courseCodes); ?>'],
	        datasets: [
	        	{
		            label: 'Average Wait per Course (minutes)',
		            data: [<?php echo implode(",", $avg_wait_per_course_waitTimes); ?>],
		            backgroundColor: [ '<?php echo implode("','", $course_wait_colors); ?>' ],
		            borderColor: '#333',
		            borderWidth: 0
	        	}
	        ]
	    },
	    options: {
	    	animation: false
		, scales: {
		    yAxes: [{
		        ticks: {
		            beginAtZero:true
		        }
		    }]
		}
	    }
	});


	var bar = document.getElementById("barChart_help_per_course");
	var myBarChart = new Chart(bar, {
	    type: 'bar',
	    data: {
	        labels: ['<?php echo implode("','", $avg_help_per_course_code); ?>'],
	        datasets: [
	        	{
		            label: 'Average Session per Course (minutes)',
		            data: [<?php echo implode(",", $avg_help_per_course_help); ?>],
		            backgroundColor: [ '<?php echo implode("','", $course_help_colors); ?>' ],
		            borderColor: '#333',
		            borderWidth: 0
	        	}
	        ]
	    },
	    options: {
	    	animation: false
		, scales: {
		    yAxes: [{
		        ticks: {
		            beginAtZero:true
		        }
		    }]
		}
	    }
	});





	// questions per course
	if (document.getElementById("barChart_questions_per_course")) {
		var bar = document.getElementById("barChart_questions_per_course");
		var myBarChart = new Chart(bar, {
		    type: 'bar',
		    data: {
		        labels: ['<?php echo implode("','", $qpc_courseName); ?>'],
		        datasets: [{
		            label: '# Questions per Course (for all time)',
		            data: [<?php echo implode(",", $qbc_count); ?>],
		            backgroundColor: [ '<?php echo implode("','", $qpc_colors); ?>' ],
		            borderColor: '#999',
		            borderWidth: 0
		        }]
		    },
		    options: {
		    	animation: false
			, scales: {
			    yAxes: [{
			        ticks: {
			            beginAtZero:true
			        }
			    }]
			}
		    }
		});
	}

</script>

<div id="footer">
	<?php echo $login_logout; ?>
</div>
	<meta http-equiv="refresh" content="300">

</body>
</html>