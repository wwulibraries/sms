<?php
session_start();

require("./db-connection.php");
// require("./functions.php");
require("./get-tutor-calendar.php");
require("./calendar2.php");

$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

$tutor_schedules = get_tutor_schedules($pdo);
$month = (int) ($_GET['month'] ? $_GET['month'] : date('m'));
$year = (int)  ($_GET['year'] ? $_GET['year'] : date('Y'));

$_GLOBALS['tab'] = "Calendar";
include("header.php");

?>
	<h3>Tutoring Center Course Coverage Calendar</h3>

	<div class="center">
		<?php echo $controls; ?>
	</div>


	<?php echo draw_calendar($month, $year, $tutor_schedules); ?>

</body>
</html>

<?php
// echo "<pre>";
// print_r($tutor_schedules);
// echo "</pre>";

?>