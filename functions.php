<?php

function gen_uid($l=10){
    # http://stackoverflow.com/questions/307486/short-unique-id-in-php
    # return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, $l);
    return substr(str_shuffle("0123456789abeghlmnqrtvwxy"), 0, $l);
}


function getColor() {
    $r = mt_rand(0,155); // generate the red component
    $g = mt_rand(0,155); // generate the green component
    $b = mt_rand(0,155); // generate the blue component
    $a = mt_rand() / mt_getrandmax();
    // $a = mt_rand() / mt_rand();
    $rgb = $r . "," . $g . "," . $b . "," . $a;
    return $rgb;
}

function getColors($number_of_colors = 1) {
    $rgba = [];
    $r = mt_rand(0,155); // generate the red component
    $g = mt_rand(0,155); // generate the green component
    $b = mt_rand(0,155); // generate the blue component

    for ($x = 1; $x <= $number_of_colors; $x++) {
        // create lighter versions of the selected color
        $a = (75 / $x) / 100;
        $rgba[] = $r . "," . $g . "," . $b . "," . $a;
    }
    return $rgba;
}


function nsrid_exists($nsrid, $pdo) {
    try {
        $stmt = $pdo->prepare('SELECT nsrid FROM sms_requests WHERE nsrid = :nsrid');
        $stmt->bindValue(':nsrid', $nsrid);
        $stmt->execute();

        if ($stmt->fetchColumn() > 0) {
            // this nsrid already exists; try again;
            return true;
        } else {
            return false;
        }
    } catch(PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }

}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        // echo "<hr>" . $k . " = " . $diff->$k;
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            // echo "<br>" . $v;
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 2);
        #return $string ? implode(', ', $string) . ' ago' : 'just now';
        return implode(', ', $string);
}

function sec2hms ($sec, $padHours = false) {
  // http://www.jonhaworth.com/articles/php/display-seconds-as-hours-minutes-seconds
  // do the hours first: there are 3600 seconds in an hour, so if we divide
  // the total number of seconds by 3600 and throw away the remainder, we're
  // left with the number of hours in those seconds
  $hours = intval(intval($sec) / 3600); 

  // start our return string with the hours (with a leading 0 if asked for)
  if ($padHours) {
    $hms = str_pad($hours, 2, "0", STR_PAD_LEFT). " ";
  }

  // dividing the total seconds by 60 will give us the number of minutes
  // in total, but we're interested in *minutes past the hour* and to get
  // this, we have to divide by 60 again and then use the remainder
  $minutes = intval(($sec / 60) % 60); 

  // add minutes to $hms (with a leading 0 if needed)
  $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). "m ";

  // seconds past the minute are found by dividing the total number of seconds
  // by 60 and using the remainder
  $seconds = intval($sec % 60); 

  // add seconds to $hms (with a leading 0 if needed)
  $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT) . "s ";

  // done!
  return $hms;

}
