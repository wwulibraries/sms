<?php

#TODO: add Google Analytics to this?

$first_name = "";
$tutors_tab = "";

$tab = $_GLOBALS['tab'];

if ($tab == "Calendar") {
	$calendar_selected = " selected ";
}

if ($tab == "Tutor") {
	$tutor_selected = " selected ";
}

if ($tab == "Queue") {
	$queue_selected = " selected ";
}

if ($tab == "Reports") {
	$reports_selected = " selected ";
}


if (isset($_SESSION['tutor_id'])) {
	$first_name = "Hello " . $_SESSION['first_name'] . ".  &nbsp; ";
	$tutor_tab = '
			<li class="tab ' . $tutor_selected . '">
				<a title="list tutors" href="tutors.php">Tutors</a>
			</li>
	';
}

$now = date('g:i:s a');

?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>WWU Tutoring Center</title>
	<link href="/sms/style.css" rel="stylesheet" type="text/css">
	<script src="https://use.fontawesome.com/c7306f4bbb.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link href="/sms/tabs.css" rel="stylesheet" type="text/css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
</head>
<body>
	<div id='wwuheader'>
		<div id="title">WWU</div>
		<h2>Tutoring Center</h2>
		<span id=wwulogo><img src=http://libweb1.library.wwu.edu/primo/images/Logo-WesternLibraries-RGB-REV.png></span>
	</div>
	<div id=nav0 class="tabs">
		<ul class="tabrow">
			<li class="tab">
				About Us
			</li>
			<li class="tab">
				Resources
			</li>
			<li class="tab <?php echo $calendar_selected; ?>">
				<a href="/sms/calendar.php">Tutor Calendar</a>
			</li>
			<li class="tab <?php echo $queue_selected; ?>">
				<a href="/sms/lookup.php">Support Queue</a>
			</li>
			<li class="tab <?php echo $reports_selected; ?>">
				<a href="/sms/reports/requests-per-day.php">Reports</a>
			</li>
			<?php echo $tutor_tab; ?>
		</ul>
		<span id="time"><?php echo $first_name; ?> Current time: <?php echo $now; ?></span>
	</div>
	<div>
		<br>
	</div>
