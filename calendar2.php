<?php

# https://davidwalsh.name/php-event-calendar

/* draws a calendar */
function draw_calendar($month, $year, $events = array()){

	$today = date("Y-m-d");                         // 2017-04-11
	$these_events[] = array();

	// echo "<pre>";
	// print_r($events);
	// echo "</pre>";

	/* draw table */
	// $calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';
	$calendar = "<div id='calendar'>";

	/* table headings */
	$headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
#	$calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

	$calendar.= '<ul class="grid"><li class="grid-cell heading">' . implode('</li><li class="grid-cell heading">',$headings) . '</li></ul>';

	/* days and weeks vars now ... */
	$running_day = date('w',mktime(0,0,0,$month,1,$year));
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	$days_in_this_week = 1;
	$day_counter = 0;
	$dates_array = array();

	/* row for week one */
#	$calendar.= '<tr class="calendar-row">';
	$calendar.= '<div class="grid">';

	/* print "blank" days until the first of the current week */
	for($x = 0; $x < $running_day; $x++):
#		$calendar.= '<td class="calendar-day-np">&nbsp;</td>';
		$calendar.= '<div class="grid-cell day"> &nbsp; </div>';
		$days_in_this_week++;
	endfor;

	/* keep going with days.... */
	for($list_day = 1; $list_day <= $days_in_month; $list_day++):
#		$calendar.= '<td class="calendar-day"><div>';
		$calendar.= '<div class="grid-cell day">';
			/* add in the day number */

			$month = str_pad($month, 2, '0', STR_PAD_LEFT);
			$list_day = str_pad($list_day, 2, '0', STR_PAD_LEFT);
			
			$event_day = $year.'-'.$month.'-'.$list_day;

			if ($event_day == $today) {
				$calendar.= '<div class="day-number today">'.$list_day.'</div>';
			} else {
				$calendar.= '<div class="day-number">'.$list_day.'</div>';
			}

			if(isset($events[$event_day])) {
				// foreach($events[$event_day] as $event) {
				foreach($events[$event_day] as $key=>$value) {

					// echo "<hr>" . $event_day;
					// echo "<br>" . $key;
					// echo "<pre>";
					// print_r($value);
					// echo "</pre>";

					$courseNumber = $key;
					$calendar .= '<div class="event"> <span class="courseNumber"> '. $courseNumber . ' </span> ';

					foreach ($value as $key2=>$value2) {
						$start_24 = $value2['start_hour'] . ':' . $value2['start_minute'];
						$start_12 =  date("g:ia", strtotime($start_24));
						$end_24 = $value2['end_hour'] . ':' . $value2['end_minute'];
						$end_12 =  date("g:ia", strtotime($end_24));

						$these_events[$event_day][$courseNumber][] = ' <span class="fromto">' . $start_12 . '-' . $end_12 . '</span>';
					}

					$calendar .= implode(",", $these_events[$event_day][$courseNumber]);

					$calendar .= "</div>";

				}
			}
			else {
				$calendar.= str_repeat('<p>&nbsp;</p>',2);
			}
#		$calendar.= '</div></td>';
		$calendar.= '</div>';
		if($running_day == 6):
#			$calendar.= '</tr>';
			$calendar.= '</div>';
			if(($day_counter+1) != $days_in_month):
#				$calendar.= '<tr class="calendar-row">';
				$calendar.= '<div class="grid">';
			endif;
			$running_day = -1;
			$days_in_this_week = 0;
		endif;
		$days_in_this_week++; $running_day++; $day_counter++;
	endfor;

	/* finish the rest of the days in the week */
	if($days_in_this_week < 8):
		for($x = 1; $x <= (8 - $days_in_this_week); $x++):
#			$calendar.= '<td class="calendar-day-np">&nbsp;</td>';
			$calendar.= '<div class="grid-cell day"> <p> &nbsp; </p> </div>';
		endfor;
	endif;

	/* final row */
#	$calendar.= '</tr>';
	$calendar.= '</div>';
	
	/* end the table */
#	$calendar.= '</table>';
	$calendar.= '</div>';

	/** DEBUG **/
	$calendar = str_replace('</td>','</td>'."\n",$calendar);
	$calendar = str_replace('</tr>','</tr>'."\n",$calendar);
	
	/* all done, return result */
	return $calendar;
}

function random_number() {
	srand(time());
	return (rand() % 7);
}

/* date settings */
$month = (int) ($_GET['month'] ? $_GET['month'] : date('m'));
$year = (int)  ($_GET['year'] ? $_GET['year'] : date('Y'));

/* select month control */
$select_month_control = '<select name="month" id="month">';
for($x = 1; $x <= 12; $x++) {
	$select_month_control.= '<option value="'.$x.'"'.($x != $month ? '' : ' selected="selected"').'>'.date('F',mktime(0,0,0,$x,1,$year)).'</option>';
}
$select_month_control.= '</select>';

/* select year control */
$year_range = 7;
$select_year_control = '<select name="year" id="year">';
for($x = ($year-floor($year_range/2)); $x <= ($year+floor($year_range/2)); $x++) {
	$select_year_control.= '<option value="'.$x.'"'.($x != $year ? '' : ' selected="selected"').'>'.$x.'</option>';
}
$select_year_control.= '</select>';

/* "next month" control */
$next_month_link = '<a href="?month='.($month != 12 ? $month + 1 : 1).'&year='.($month != 12 ? $year : $year + 1).'" class="control">Next Month &gt;&gt;</a>';

/* "previous month" control */
$previous_month_link = '<a href="?month='.($month != 1 ? $month - 1 : 12).'&year='.($month != 1 ? $year : $year - 1).'" class="control">&lt;&lt; 	Previous Month</a>';


/* bringing the controls together */
$controls = '<form method="get">'.$select_month_control.$select_year_control.'&nbsp;<input type="submit" name="submit" value="Go" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$previous_month_link.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$next_month_link.' </form>';

// /* get all events for the given month */
// $events = array();
// $query = &quot;SELECT title, DATE_FORMAT(event_date,'%Y-%m-%D') AS event_date FROM events WHERE event_date LIKE '$year-$month%'&quot;;
// $result = mysql_query($query,$db_link) or die('cannot get results!');
// while($row = mysql_fetch_assoc($result)) {
// 	$events[$row['event_date']][] = $row;
// }

// echo '<h2 style="float:left; padding-right:30px;">'.date('F',mktime(0,0,0,$month,1,$year)).' '.$year.'</h2>';
// echo '<div style="float:left;">'.$controls.'</div>';
// echo '<div style="clear:both;"></div>';
// echo draw_calendar($month,$year,$events);
// echo '<br /><br />';

?>