
# PURPOSE
 Enable patrons to request information or assistance via SMS

# OVERVIEW
We setup a free https://www.twilio.com account, and in the "Manage Numbers" section, pointed the "Messaging" URL to http://dev0.library.wwu.edu/sms/reply.php 

reply.php has two functions at this point in time:
- if the Body of the SMS message equals "hours", then we lookup the Library's hours via the libcal api.
- if the Body equals a table number (such as "a1", "a2", "a3" or "a4"), then we add that to a queue of people who are requesting on-site assistance.

 The lookup.php script automatically refreshes every 45 seconds, and shows the list of people waiting for assistance.  This script could put on a big screen in a public area to let patrons know how many people are ahead of them.

The update.php script listens for a table number (e.g. "http://dev0.library.wwu.edu/sms/update.php?table=a3"), and then removes that table from the queue by changing the status from 1 to 2.
This obviously has no security, but for our purposes, that is okay at this point.    The system also includes a nsrid (non-sequential record id), which could add some security to this update script, in case there is concern about unauthorized users changing the status of a request.  
The update.php script also logs the status change in the status_updates table, which could be used later for analytics (how long did each request stay in a certain status).
This is not currently done in the most elegant way, but it works for now.

# TODO
 - I'd like to rewrite this using nodejs (maybe socket.io) to replace the meta refresh to facilitate real-time interactivity.

# TECHNICAL

We created a MySQL database and then created a table like this:

	CREATE TABLE `sms_requests` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `from` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
	  `msg` text COLLATE utf8_unicode_ci NOT NULL,
	  `version` TINYINT DEFAULT '1',
	  `status` TINYINT DEFAULT '1',
	  `created` datetime DEFAULT NULL,
	  `modified` datetime DEFAULT NULL,
	  `nsrid` varchar(13),
	   PRIMARY KEY (`id`),
	   UNIQUE (`nsrid`),
	   FOREIGN KEY (`status`) REFERENCES status_codes (status_id) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci

	CREATE TABLE `status_codes` (
	  `status_id` TINYINT NOT NULL AUTO_INCREMENT,
	  `title` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
	   PRIMARY KEY (`status_id`),
	   UNIQUE (`title`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci

	INSERT INTO status_codes (status_id, title) VALUES (1, 'New'), (2, 'In progress'), (3, 'Completed'), (4, 'Canceled');

	CREATE TABLE `status_updates` (
	  `request_id` INT NOT NULL,
	  `status_id` TINYINT NOT NULL,
	  `created` datetime DEFAULT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci


and then created a MySQL user that only has INSERT and SELECT access to the table below.

	CREATE USER 'web_user'@'localhost' IDENTIFIED BY 'password-goes-here';
	GRANT INSERT, SELECT, UPDATE PRIVILEGES ON database-name.sms_requests TO 'web-user'@'localhost';
	FLUSH PRIVILEGES;

and then, create a file named "db-connection.php" in this folder, and populate the following variables:

	<?php

	$dbServer = "localhost";
	$dbName = " ";
	$dbUserName = " ";
	$dbPassword = " ";

	?>
