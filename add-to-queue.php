<?php
session_start();
$sid = session_id();
$sid_clean = filter_var($sid, FILTER_SANITIZE_STRING);
$response = "";



if(isset($_POST['seat'])) {
	$seat = strtoupper(trim($_POST['seat']));
	$seat = filter_var($seat, FILTER_SANITIZE_STRING);
} else {
	echo " seat is required. ";
	exit();
}

if(isset($_POST['course'])) {
	$course = $_POST['course'];
	$course = filter_var($course, FILTER_SANITIZE_STRING);
} else {
	echo " course is required. ";
	exit();
}

$from = "web";

require("./db-connection.php");
require("./functions.php");
require("./tables-positions.php");
require("./subject-crn.php");

$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

$version = 1;			// Feb 2016

try {

	$valid_tables_positions = get_tables_positions($pdo);
	$valid_subjects_crns = get_subjects_crns($pdo);

	$subject_crn_id = array_search($course, $valid_subjects_crns); // $key = 2;
	$subject_crn_id = intval($subject_crn_id);


	if(!in_array($seat, $valid_tables_positions)) {
		# respond with an error message;
		$response = "Invalid table/position.";
		$response .= " \nPlease send your table/position number (e.g. '3a') for on-site support.";
	} else {

		# insert the request into the database;

		$version = 1;
		$status = 1;		# new
	      	$now = date('Y-m-d H:i:s');
		$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

		do {
			$nsrid = gen_uid(6);
	      	}
	      		while (nsrid_exists($nsrid, $pdo));

	      	# insert the request into the sms_requests table
	      	$sql = 'INSERT INTO sms_requests (`created`, `table_position`, `from`, `version`,  `nsrid`, `status`,`session_id`, `subject_crn`) VALUES (:created, :table_position, :from, :version, :nsrid, :status, :session_id, :subject_crn)';
		$stmt = $pdo->prepare($sql);
		$stmt->bindValue(':created', $now);
		$stmt->bindValue(':table_position', $seat);
		$stmt->bindValue(':from', $from);
		$stmt->bindValue(':version', $version);
		$stmt->bindValue(':status', $status);
		$stmt->bindValue(':nsrid', $nsrid);
		$stmt->bindValue(':session_id', $sid);
		$stmt->bindValue(':subject_crn', $subject_crn_id);
		$inserted = $stmt->execute();

		if ($inserted) {

			# insert this in the status_updates table - we can use this to analyze wait times later;
			# $request_id = PDO::lastInsertId(); 
			$request_id = $pdo->lastInsertId(); 
		      	$sql = 'INSERT INTO status_updates (`created`, `request_id`, `status_id`) VALUES (:created, :request_id, :status_id)';
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':created', $now);
			$stmt->bindValue(':request_id', $request_id);
			$stmt->bindValue(':status_id', $status);
			$status_inserted = $stmt->execute();
			if (!$status_inserted) {
				# report this error;
			}

			# how many people are in front of this patron (for today only)?
			$sql = 'SELECT COUNT(*) FROM sms_requests  WHERE status = 1 AND DATE(`created`) = DATE(NOW()) ';
			if ($res = $pdo->query($sql)) {
				$num_requests = $res->fetchColumn();
				$num_requests = $num_requests - 1;		# REMOVE THIS request from the count
				if ($num_requests == 0) {
					$queue_msg = " Thanks; you are the first in line.";
				} elseif ($num_requests == 1) {
					$queue_msg = " Thanks; there is 1 request ahead of you.";
				} else {
					$queue_msg = " Thanks; there are " . $num_requests . " requests ahead of you.";
				}
			}
			$response = $queue_msg; 

			$response .= "  You will now be redirected back to the queue. ";
		} else {
			$response = "An error occured.";
		}
	}		
} catch(PDOException $e) {
    $response = 'ERROR: ' . $e->getMessage();
}

echo $response;
?>

<meta http-equiv="refresh" content="1;url=lookup.php" />
