<?php

function get_subjects_crns($pdo, $output_type = '') {
	$output = "";
	$rowNumber = 0;

	try {
		// $pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);
		// get a list of active requests from today (ignore anything created prior to today);
		$stmt = $pdo->prepare('SELECT `record_id`, `subject_number`, `subject`, `course` FROM subject_crn ');
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		// $row_count = $stmt->rowCount();

		if($rows !== false) {
			foreach($rows as $row) {
				$rowNumber++;
				$mod = $rowNumber % 10;
				// $output[] = $row['subject'] . $row['course'];
				if ($output_type == "checkbox") {
					if ($mod == 1) {
						$pre = "<div class='grid-cell'>";
					} else {	
						$pre = " ";
					}

					if ($mod == 0) {
						$post = "</div>";
					} else {	
						$post = "";
					}

					$output[] = $pre . "<div><label for=cb_" . $row['record_id'] . "> <input type=checkbox name=courses[] id=cb_" . $row['record_id'] . " value='" . $row['record_id'] . "'> " . $row['subject'] . " " . $row['course'] . " </label> </div>" . $post;							
				} else {
					$output[$row['record_id']] =  $row['subject'] . "" . $row['course'];
				}

			}
		}

		$pdo = null;

		$response = $output;

	} catch(PDOException $e) {
	    	$response = 'ERROR: ' . $e->getMessage();
	}

	return $response;
}
