<?php
session_start();
$response = "";
$tutor_id = 0;

if (isset($_SESSION['tutor_id'])) {
	# the user is logged-in;
	$tutor_id = intval($_SESSION['tutor_id']);
	$logged_in = true;
} else {
	echo "You must be logged-in to view this page.";
	exit();
}

$_GLOBALS['tab'] = "Tutor";
require("./header.php");
require("./db-connection.php");
require("./functions.php");
require("./subject-crn.php");

$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

if (!isset($_POST['submit'])) {
	$subjects_crns = get_subjects_crns($pdo, "checkbox");
	$subjects_crns_form = implode("", $subjects_crns);

	$form = '<form method=post>
		<div> <label for="first_name">First Name</label> <input type=text name=first_name id=first_name required> </div>
		<div> <label for="last_name">Last Name</label> <input type=text name=last_name id=last_name required> </div>
		<div> <label for="email_address">Email Address</label> <input type=email name=email_address id=email_address required> </div>
		<div> <label for="password">Password</label> <input type=password name=password id=password required placeholder="password"> </div>
		<div> <label for="password">(again)</label> <input type=password name=password2 id=password2 required placeholder="confirm password"> </div>
		<div> 
			<fieldset> 
				<legend> Which subject/courses are you qualified to tutor? </legend> 
				<div class="grid">' . $subjects_crns_form . '</div>
			</fieldset>
		</div>
		<div> <input type=submit name=submit value=Submit> </div>
		</form>
	';

	echo $form;
	exit();
} else {

	$fname = trim($_POST['first_name']);
	$fname = filter_var($fname, FILTER_SANITIZE_STRING);

	$lname = trim($_POST['last_name']);
	$lname = filter_var($lname, FILTER_SANITIZE_STRING);

	$email = trim($_POST['email_address']);
	$email = filter_var($email, FILTER_SANITIZE_STRING);

	$password = trim($_POST['password']);
	$password = filter_var($password, FILTER_SANITIZE_STRING);

	$password2 = trim($_POST['password2']);
	$password2 = filter_var($password2, FILTER_SANITIZE_STRING);

	if ($password != $password2) {
		echo "<div>Passwords did not match.</div>";
		exit();
	}

	$password_hash = password_hash($password, PASSWORD_DEFAULT);

	if(isset($_POST['courses'])) {
	} else {
		echo " <div> You must select at least one course.</div> ";
		exit();
	}

	try {
	      	$now = date('Y-m-d H:i:s');
	      	$status = 1;

	      	# insert the request into the sms_requests table
	      	$sql = 'INSERT INTO tutors (`date_added`, `first_name`, `last_name`, `email_address`,  `password_hash`, `status`, `added_by`) VALUES (:date_added, :first_name, :last_name, :email_address, :password_hash, :status, :added_by)';
		$stmt = $pdo->prepare($sql);
		$stmt->bindValue(':date_added', $now);
		$stmt->bindValue(':first_name', $fname);
		$stmt->bindValue(':last_name', $lname);
		$stmt->bindValue(':email_address', $email);
		$stmt->bindValue(':status', $status);
		$stmt->bindValue(':added_by', $tutor_id);
		$stmt->bindValue(':password_hash', $password_hash);
		$inserted = $stmt->execute();

		if ($inserted) {
			$response .= "  Thanks :) ";
			$tutor_id = $pdo->lastInsertId();

		} else {
			$response = "An error occured.";
		}
	} catch(PDOException $e) {
	    $response = 'ERROR: ' . $e->getMessage();
	}

	if ($tutor_id > 0) {
		foreach($_POST['courses'] as $course) {
			$subject_crn = filter_var($course, FILTER_SANITIZE_STRING);

			try {
			      	$sql = 'INSERT INTO tutor_course (tutor_id, subject_crn) VALUES (:tutor_id, :subject_crn)';
				$stmt = $pdo->prepare($sql);
				$stmt->bindValue(':tutor_id', $tutor_id);
				$stmt->bindValue(':subject_crn', $subject_crn);
				$inserted = $stmt->execute();

				if ($inserted) {
					$response .= "<br> subject " . $subject_crn . " added to tutor profile. ";
				} else {
					$response = "An error occured.";
				}
			} catch(PDOException $e) {
			    $response = 'ERROR: ' . $e->getMessage();
			}
		}
	}


	echo $response;
	echo '<meta http-equiv="refresh" content="1;url=lookup.php" />';
}


?>