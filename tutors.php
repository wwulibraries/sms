<?php
session_start();
$logged_in = false;	
$tutor_id = 0;

if (isset($_SESSION['tutor_id'])) {
	# the user is logged-in;
	$tutor_id = intval($_SESSION['tutor_id']);
	$logged_in = true;
} else {
	echo "You must <a href=login.php>log-in</a> to view this page.";
	exit();
}


require("./db-connection.php");
require("./functions.php");
require("./subject-crn.php");
require("./list-tutors.php");

$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

$valid_subjects_crns = get_subjects_crns($pdo);
$subject_crn_form = implode("<option>", $valid_subjects_crns);

$tutors = list_tutors($pdo);
$tutors_html = implode(" ", $tutors);

$_GLOBALS['tab'] = "Tutor";
include("header.php");

?>
	<button><a title='add new tutor' href='create-new-tutor.php'>Add new tutor</a></button>
	<button><a title='add new schedule' href='schedule-tutor.php'>Add new schedule</a></button>

       	<div class="grid">
       		<div class="grid-cell">
       			Tutors:
       			<table>
       				<tr>
       					<td>ID</td>
       					<td>First</td>
       					<td>Last</td>
       					<td>Email</td>
       					<td>Added</td>
       					<td>Courses</td>
       				</tr>
				<?php echo $tutors_html; ?>
			</table>
		</div>
	</div>
</body>
</html>