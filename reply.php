<?php
session_start();
$sid = session_id();
$sid_clean = filter_var($sid, FILTER_SANITIZE_STRING);
$response = "";
$subject_crn_id = 0;

if(isset($_REQUEST['Body'])) {

	require("./db-connection.php");
	require("./functions.php");
	require("./tables-positions.php");
	require("./subject-crn.php");

	$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

	$version = 1;			// Feb 2016

	try {

		$msg = strtoupper(trim($_REQUEST['Body']));
		$msg = filter_var($msg, FILTER_SANITIZE_STRING);

		$from = $_REQUEST['From'];
		$from = filter_var($from, FILTER_SANITIZE_STRING);

		$valid_tables_positions = get_tables_positions($pdo);
		$valid_subjects_crns = get_subjects_crns($pdo, 'array');

		// echo "<pre>";
		// print_r($valid_subjects_crns);
		// echo "</pre>";

		$msg_no_space = str_replace(' ', '', $msg);
		$subject_crn_id = array_search($msg_no_space, $valid_subjects_crns); // $key = 2;
		$subject_crn_id = intval($subject_crn_id);

		if ($subject_crn_id > 0) {

		// echo $subject_crn_id;

			# do we already have an active conversation with this number and session id?
			# if so, then make sure we have the table/position and the subject/crn
			$stmt = $pdo->prepare('SELECT `id`, `from`, `table_position`, `created`, `subject_crn` FROM sms_requests  WHERE `status` = 1  AND `from` = :from AND `session_id` = :session_id ORDER BY `created` ');
			$stmt->bindValue(':session_id', $sid_clean);
			$stmt->bindValue(':from', $from);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$row_count = $stmt->rowCount();
			$now = date('g:i:s a');
			if($rows !== false) {
				foreach($rows as $row) {
					$subject_crn = $row['subject_crn'];
					$response = $subject_crn;
					if ($subject_crn == NULL) {
						# is the user sending us their subject_crn?  if so, update this record
						$sql = '
							UPDATE 
								`sms_requests` 
							SET 
								`subject_crn` = :subject_crn, 
								`modified` = :modified 
							WHERE 
								`status` = 1 
								AND `session_id` = :session_id  
								AND `from` = :from
						';

						$stmt = $pdo->prepare($sql);
						$stmt->bindValue(':modified', $now);
						$stmt->bindValue(':subject_crn', $subject_crn_id);
						$stmt->bindValue(':session_id', $sid_clean);
						$stmt->bindValue(':from', $from);
						$updated = $stmt->execute();
						if ($updated) {
							$response = " Thanks; we've got it. ";
						}
					}
				}
			} 
		} else {
			# we have not yet created a response, so that means this is a new request;

			if(!in_array($msg, $valid_tables_positions)) {
				# respond with an error message;
				$response = "Invalid table/seat.";
				$response .= " \nPlease send your table/seat number (e.g. '3a') for on-site support.";
			} else {

				if ($msg == "hours") {
					$url = "https://api3.libcal.com/api_hours_today.php?iid=388&lid=0&format=json";

					$curlSession = curl_init();
					curl_setopt($curlSession, CURLOPT_URL, $url);
					curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
					curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
					$data = curl_exec($curlSession);

					curl_close($curlSession);
					$json_array = json_decode($data, true);
					$response = $json_array["locations"][0]["rendered"];

				} else {
					
					# insert the request into the database;

					$version = 1;
					$status = 1;		# new
				      	$now = date('Y-m-d H:i:s');
					$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

					do {
						$nsrid = gen_uid(6);
				      	}
				      		while (nsrid_exists($nsrid, $pdo));

				      	# insert the request into the sms_requests table
				      	$sql = 'INSERT INTO sms_requests (`created`, `table_position`, `from`, `version`,  `nsrid`, `status`,`session_id`) VALUES (:created, :table_position, :from, :version, :nsrid, :status, :session_id)';
					$stmt = $pdo->prepare($sql);
					$stmt->bindValue(':created', $now);
					$stmt->bindValue(':table_position', $msg);
					$stmt->bindValue(':from', $from);
					$stmt->bindValue(':version', $version);
					$stmt->bindValue(':status', $status);
					$stmt->bindValue(':nsrid', $nsrid);
					$stmt->bindValue(':session_id', $sid);
					$inserted = $stmt->execute();

					if ($inserted) {

						# insert this in the status_updates table - we can use this to analyze wait times later;
						# $request_id = PDO::lastInsertId(); 
						$request_id = $pdo->lastInsertId(); 
					      	$sql = 'INSERT INTO status_updates (`created`, `request_id`, `status_id`) VALUES (:created, :request_id, :status_id)';
						$stmt = $pdo->prepare($sql);
						$stmt->bindValue(':created', $now);
						$stmt->bindValue(':request_id', $request_id);
						$stmt->bindValue(':status_id', $status);
						$status_inserted = $stmt->execute();
						if (!$status_inserted) {
							# report this error;
						}

						# how many people are in front of this patron (for today only)?
						$sql = 'SELECT COUNT(*) FROM sms_requests  WHERE status = 1 AND DATE(`created`) = DATE(NOW()) ';
						if ($res = $pdo->query($sql)) {
							$num_requests = $res->fetchColumn();
							$num_requests = $num_requests - 1;		# REMOVE THIS request from the count
							if ($num_requests == 0) {
								$queue_msg = " Thanks; you are the first in line.";
							} elseif ($num_requests == 1) {
								$queue_msg = " Thanks; there is 1 request ahead of you.";
							} else {
								$queue_msg = " Thanks; there are " . $num_requests . " requests ahead of you.";
							}
						}
						$response = $queue_msg; 

						$response .= "  Visit http://dev0.library.wwu.edu/sms/lookup.php to see the queue. ";
						$response .= " \nWhat is your subject/course number (e.g. CHEM 121)? ";
					} else {
						$response = "An error occured.";
					}

				}

			}
		}		
	} catch(PDOException $e) {
	    $response = 'ERROR: ' . $e->getMessage();
	}

	// now greet the sender
	header("content-type: text/xml");
	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

}


?>
<Response>
	<Message><?php echo $response ?></Message>
</Response>