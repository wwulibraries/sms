<?php

function avg_wait($pdo) {
	$output = "";

	try {
		$sql = ' 
			SELECT AVG(TIMESTAMPDIFF(SECOND, A.created, B.created )) AS timedifference, A.created as created, B.created as createdB
			FROM status_updates A CROSS JOIN status_updates B
			WHERE B.status_id = 2 AND A.status_id = 1 AND B.request_id = A.request_id
		';

		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		// $row_count = $stmt->rowCount();

		if($rows !== false) {
			foreach($rows as $row) {
				$diff_pretty = sec2hms( $row['timedifference'] );
				$output = $diff_pretty;
			}
		}

		$response = $output;

		$pdo = null;

	} catch(PDOException $e) {
	    	$response = 'ERROR: ' . $e->getMessage();
	}

	return $response;
}

function get_help_times($pdo) {
	$output = "";
	try {
		$sql = ' 
			SELECT AVG(TIMESTAMPDIFF(SECOND, A.created, B.created )) AS timedifference, A.created as created, B.created as createdB
			FROM status_updates A CROSS JOIN status_updates B
			WHERE B.status_id = 3 AND A.status_id = 2 AND B.request_id = A.request_id
		';

		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		// $row_count = $stmt->rowCount();

		if($rows !== false) {
			foreach($rows as $row) {
				$diff_pretty = sec2hms( $row['timedifference'] );
				$output = $diff_pretty;
			}
		}

		$response = $output;

		$pdo = null;

	} catch(PDOException $e) {
	    	$response = 'ERROR: ' . $e->getMessage();
	}

	return $response;
}

function avg_wait_per_day($pdo) {
	$output = "";
	try {
		$sql = ' 
			SELECT AVG(TIMESTAMPDIFF(MINUTE, A.created, B.created )) AS timedifference, DATE(A.created) as created
			FROM status_updates A CROSS JOIN status_updates B
			WHERE B.status_id = 2 AND A.status_id = 1 AND B.request_id = A.request_id
			GROUP BY DATE(A.created) 
		';

		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		// $row_count = $stmt->rowCount();

		if($rows !== false) {
			foreach($rows as $row) {
				$output[$row['created']] =  $row['timedifference'];
			}
		}

		$response = $output;

		$pdo = null;

	} catch(PDOException $e) {
	    	$response = 'ERROR: ' . $e->getMessage();
	}

	return $response;
}

function avg_help_per_day($pdo) {
	$output = "";
	try {
		$sql = ' 
			SELECT AVG(TIMESTAMPDIFF(MINUTE, A.created, B.created )) AS timedifference, DATE(A.created) as created
			FROM status_updates A CROSS JOIN status_updates B
			WHERE B.status_id = 3 AND A.status_id = 2 AND B.request_id = A.request_id
			GROUP BY DATE(A.created) 
		';

		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		// $row_count = $stmt->rowCount();

		if($rows !== false) {
			foreach($rows as $row) {
				$output[$row['created']] =  $row['timedifference'];
			}
		}

		$response = $output;

		$pdo = null;

	} catch(PDOException $e) {
	    	$response = 'ERROR: ' . $e->getMessage();
	}

	return $response;
}


function avg_wait_per_course($pdo) {
	$output = "";
	try {
		$sql = ' 
			SELECT AVG(TIMESTAMPDIFF(MINUTE, A.created, B.created )) AS timedifference, DATE(A.created) as created, sms_requests.id, sms_requests.subject_crn, CONCAT(subject_crn.subject, " ", subject_crn.course) as courseNumber
			FROM status_updates A
			CROSS JOIN status_updates B
			JOIN sms_requests ON sms_requests.id = A.request_id
			JOIN subject_crn ON subject_crn.record_id = sms_requests.subject_crn
			WHERE B.status_id = 2 AND A.status_id = 1 AND B.request_id = A.request_id
		 	GROUP BY sms_requests.subject_crn 	
		';

		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		// $row_count = $stmt->rowCount();

		if($rows !== false) {
			foreach($rows as $row) {
				$courseNumber = $row['courseNumber'];
				$output[$courseNumber] = $row['timedifference'];
			}
		}

		$response = $output;

		$pdo = null;

	} catch(PDOException $e) {
	    	$response = 'ERROR: ' . $e->getMessage();
	}

	return $response;
}

function avg_help_per_course($pdo) {
	$output = "";
	try {
		$sql = ' 
			SELECT AVG(TIMESTAMPDIFF(MINUTE, A.created, B.created )) AS timedifference, DATE(A.created) as created, sms_requests.id, sms_requests.subject_crn, CONCAT(subject_crn.subject, " ", subject_crn.course) as courseNumber
			FROM status_updates A
			CROSS JOIN status_updates B
			JOIN sms_requests ON sms_requests.id = A.request_id
			JOIN subject_crn ON subject_crn.record_id = sms_requests.subject_crn
			WHERE B.status_id = 3 AND A.status_id = 2 AND B.request_id = A.request_id
		 	GROUP BY sms_requests.subject_crn 	
		';

		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		// $row_count = $stmt->rowCount();

		if($rows !== false) {
			foreach($rows as $row) {
				$courseNumber = $row['courseNumber'];
				$output[$courseNumber] = $row['timedifference'];
			}
		}

		$response = $output;

		$pdo = null;

	} catch(PDOException $e) {
	    	$response = 'ERROR: ' . $e->getMessage();
	}

	return $response;
}




function questions_per_course($pdo) {
	$output = "";
	try {
		$sql = ' 
			SELECT COUNT(*) as count, CONCAT(subject_crn.subject, " ", subject_crn.course) as courseNumber
			FROM sms_requests
			JOIN subject_crn ON subject_crn.record_id = sms_requests.subject_crn
			GROUP BY subject_crn
		';

		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if($rows !== false) {
			foreach($rows as $row) {
				$count = $row['count'];
				$courseNumber = $row['courseNumber'];
				$output[$courseNumber] = $count;
			}
		}

		$response = $output;
		$pdo = null;
	} catch(PDOException $e) {
	    	$response = 'ERROR: ' . $e->getMessage();
	}
	return $response;
}




function questions_per_subject_per_day($pdo) {
	$output = "";
	try {
		$sql = ' 
			SELECT COUNT(*) as count, DATE(sms_requests.created) as created, CONCAT(subject_crn.subject, " ", subject_crn.course) as courseNumber
			FROM sms_requests
			JOIN subject_crn ON subject_crn.record_id = sms_requests.subject_crn
			GROUP BY subject_crn
			ORDER BY created
		';

		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		// $row_count = $stmt->rowCount();

		if($rows !== false) {
			foreach($rows as $row) {
				$date = $row['created'];
				$count = $row['count'];
				$courseNumber = $row['courseNumber'];
				$output[$date][$courseNumber] = $count;
			}
		}

		$response = $output;

		$pdo = null;

	} catch(PDOException $e) {
	    	$response = 'ERROR: ' . $e->getMessage();
	}

	return $response;
}


