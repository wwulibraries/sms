<?php
session_start();
$response = "";
$tutor_id = 0;

if (isset($_GET['tid'])) {
	$tid = intval($_GET['tid']);
}

if (isset($_SESSION['tutor_id'])) {
	# the user is logged-in;
	$tutor_id = intval($_SESSION['tutor_id']);
	$logged_in = true;
} else {
	echo "You must be logged-in to view this page.";
	exit();
}

$_GLOBALS['tab'] = "Tutor";
require("./header.php");
require("./db-connection.php");
require("./functions.php");

$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

$list_o_tutors = "";
$this_selected =  "";

$stmt = $pdo->prepare(' SELECT tutors.tutor_id, `first_name`, `last_name`, `email_address`, `date_added` FROM tutors WHERE status = 1 ');
$stmt->execute();
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
if($rows !== false) {
	foreach($rows as $row) {
		$tutor_id = $row['tutor_id'];
		$date_added = $row['date_added'];
		$email = $row['email_address'];
		$fname = $row['first_name'];
		$lname = $row['last_name'];
		if ($tid == $tutor_id) {
			$this_selected = " SELECTED ";
		} else {
			$this_selected = " ";
		}
		$list_o_tutors .= "<option " . $this_selected . " value='" . $tutor_id . "'>" . $fname . " " . $lname . "</option>";
	}
}


$today = date("Y-m-d");                       // 2017-01-09 

if (!isset($_POST['submit'])) {
	$form = '<form method=post>
		<h2>Add Tutor Schedule</h2>
		<div> <label for="tutor">Tutor</label> <select required name="selected_tutor_id" name="selected_tutor_id"><option></option> ' . $list_o_tutors . '</select></div>
		<div> <label for="date">Date</label> <input required type="date" id="date" name="date" value="' . $today . '"> </div>
		<div> <label for="start">Start Time <select id="start" name="start"><option value=07>7 AM</option><option value=08>8 AM</option><option value=09>9 AM</option><option value=10>10 AM</option><option value=11>11 AM</option><option value=12>12 PM</option><option value=13>1 PM</option><option value=14>2 PM</option><option value=15>3 PM</option><option value=16>4 PM</option><option value=17>5 PM</option><option value=18>6 PM</option><option value=19>7 PM</option><option value=20>8 PM</option><option value=21>9 PM</option><option value=22>10 PM</option><option value=23>11 PM</option></select> 
			 <select id="startmin" name="startmin"><option>00</option><option>15</option><option>30</option><option>45</option></select> 
			  </div>
		<div> <label for="end">End Time <select id="end" name="end"><option value=07>7 AM</option><option value=08>8 AM</option><option value=09>9 AM</option><option value=10>10 AM</option><option value=11>11 AM</option><option value=12>12 PM</option><option value=13>1 PM</option><option value=14>2 PM</option><option value=15>3 PM</option><option value=16>4 PM</option><option value=17>5 PM</option><option value=18>6 PM</option><option value=19>7 PM</option><option value=20>8 PM</option><option value=21>9 PM</option><option value=22>10 PM</option><option value=23>11 PM</option></select>   
			 <select id="endmin" name="endmin"><option>00</option><option>15</option><option>30</option><option>45</option></select> 
		<div> <input type=submit name=submit value=Submit> </div>
		</form>
	';

	echo $form;

	echo '
	<script>
		$(document).ready(function(){

		});
	</script>

	';

} else {

	$selected_tutor_id = intval($_POST['selected_tutor_id']);
	$date = trim($_POST['date']);
	$date = filter_var($date, FILTER_SANITIZE_STRING);

	$start = trim($_POST['start']);
	$start = filter_var($start, FILTER_SANITIZE_STRING);

	$startmin = trim($_POST['startmin']);
	$startmin = filter_var($startmin, FILTER_SANITIZE_STRING);

	$end = trim($_POST['end']);
	$end = filter_var($end, FILTER_SANITIZE_STRING);

	$endmin = trim($_POST['endmin']);
	$endmin = filter_var($endmin, FILTER_SANITIZE_STRING);

	$start_db = $date . " " . $start . ":" . $startmin . ":00";
	$end_db = $date . " " . $end . ":" . $endmin . ":00";

	try {
	      	$now = date('Y-m-d H:i:s');

	      	# insert the request into the sms_requests table
	      	$sql = 'INSERT INTO tutor_schedule (`created`, `tutor_id`, `start`, `end`, `created_by`) VALUES (:created, :selected_tutor_id, :start_datetime, :end_datetime, :created_by)';
		$stmt = $pdo->prepare($sql);
		$stmt->bindValue(':created', $now);
		$stmt->bindValue(':selected_tutor_id', $selected_tutor_id);
		$stmt->bindValue(':start_datetime', $start_db);
		$stmt->bindValue(':end_datetime', $end_db);
		$stmt->bindValue(':created_by', $tutor_id);
		$inserted = $stmt->execute();

		if ($inserted) {
			$response .= "  Thanks :) ";
		} else {
			$response = "An error occured.";
		}
	} catch(PDOException $e) {
	    $response = 'ERROR: ' . $e->getMessage();
	}

	echo $response;
	echo '<meta http-equiv="refresh" content="1;url=tutors.php" />';
}

$pdo = null;

?>