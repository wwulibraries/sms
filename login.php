<?php
session_start();

require("./db-connection.php");
require("./functions.php");

$tab = "Tutor";
require("./header.php");

if ( (!isset($_POST['login_email'])) || (!isset($_POST['login_password'])) ) { 
	# show the login form;
	echo '
	<form method="post">
		<div>
			<input type=email required name=login_email title="your email address" placeholder="your email address">
		</div>
		<div>
			<input type=password required name=login_password title="your password" placeholder="your password">
		</div>
		<div>
			<input type=submit name=submit value="Submit">
		</div>
	</form>
	';
}

# TODO: log the login attempts

$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

$login_email = $_POST['login_email'];
$login_email = filter_var($login_email, FILTER_SANITIZE_STRING);		

$login_password = $_POST['login_password'];
$login_password = filter_var($login_password, FILTER_SANITIZE_STRING);		


try {
	$stmt = $pdo->prepare(' SELECT `tutor_id`, `first_name`, `last_name`, `email_address`, `password_hash` FROM tutors WHERE `status` = 1 AND `email_address` = :login_email ');
	$stmt->bindValue(':login_email', $login_email);
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

	// $row_count = $stmt->rowCount();

	if ($rows === false) {
		echo  "Invalid email or password.";

	      	$sql = 'INSERT INTO tutor_logins (`email`, `result`) VALUES (:email, 0)';
		$stmt = $pdo->prepare($sql);
		$stmt->bindValue(':email', $login_email);
		$status_inserted = $stmt->execute();
		if (!$status_inserted) {
			# TODO: log / report this error;
		}
		sleep(3);
		exit();
	} else {
		foreach($rows as $row) {
			$password_hash = $row['password_hash'];
			if (password_verify($login_password, $password_hash)) {
				$tutor_id = $row['tutor_id'];
				$first_name = $row['first_name'];
				$last_name = $row['last_name'];
				$email_address = $row['email_address'];
				$_SESSION['tutor_id'] = $tutor_id;
				$_SESSION['first_name'] = $first_name;
				$_SESSION['last_name'] = $last_name;
				$_SESSION['email_address'] = $email_address;
				echo "<div>Hello " . $first_name . ". </div>";
				echo '<meta http-equiv="refresh" content="3;url=lookup.php" />';				

			      	$sql = 'INSERT INTO tutor_logins (`email`, `result`, `tutor_id`) VALUES (:email, 1, :tutor_id)';
				$stmt = $pdo->prepare($sql);
				$stmt->bindValue(':email', $login_email);
				$stmt->bindValue(':tutor_id', $tutor_id);
				$status_inserted = $stmt->execute();
				if (!$status_inserted) {
					# TODO: log / report this error;
				}

			} else {
				echo  "Invalid email or password.";				
			      	$sql = 'INSERT INTO tutor_logins (`email`, `result`) VALUES (:email, 0)';
				$stmt = $pdo->prepare($sql);
				$stmt->bindValue(':email', $login_email);
				$status_inserted = $stmt->execute();
				if (!$status_inserted) {
					# TODO: log / report this error;
				}

				sleep(3);
				exit();
			}

		}
	}

	$pdo = null;

} catch(PDOException $e) {
    	$response = 'ERROR: ' . $e->getMessage();
}
