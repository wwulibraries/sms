<?php
session_start();

$logged_in = false;	
$tutor_id = 0;

if (isset($_SESSION['tutor_id'])) {
	# the user is logged-in;
	$tutor_id = intval($_SESSION['tutor_id']);
	$logged_in = true;
}

require("./db-connection.php");
require("./functions.php");
require("./tables-positions.php");

$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

$msg = "";
if (isset($_POST['nsrid'])) {
	$nsrid = strtoupper(trim($_POST['nsrid']));
	$nsrid = filter_var($nsrid, FILTER_SANITIZE_STRING);		
} else {
	echo "<div>You must select one of the requests.</div>";
	exit();
}

# status codes : 1 = waiting, 2 = being served, 3 = closed
$new_status = "";

if (isset($_POST['claim'])) {
	$new_status = 2;
}	

if (isset($_POST['close'])) {
	$new_status = 3;
}	

if (isset($_POST['unclaim'])) {
	$new_status = 1;
}	

try {

     	$now = date('Y-m-d H:i:s');

	$sql = '
		UPDATE 
			`sms_requests` 
		SET 
			`status` = :status, 
			`modified` = :modified 
		WHERE 
			`nsrid` = :nsrid  
			AND DATE(`created`) = DATE(NOW())  
	';			
	$stmt = $pdo->prepare($sql);
	$stmt->bindValue(':modified', $now);
	$stmt->bindValue(':status', $new_status);
	$stmt->bindValue(':nsrid', $nsrid);

	$updated = $stmt->execute();

	if ($updated) {

		# what is the best way to get the id of this record?
		# until I find a better way, I guess I'll have to do this
	
	      	$sql = 'SELECT `id` FROM sms_requests WHERE `nsrid` = :nsrid AND `modified` = :modified ';
		$stmt = $pdo->prepare($sql);
		$stmt->bindValue(':modified', $now);
		$stmt->bindValue(':nsrid', $nsrid);
		$stmt->execute();
		$request_id = $stmt->fetchColumn(); 

		$response .= "<div> Thanks </div>";

		# insert this in the status_updates table - we can use this to analyze wait times later;
		# $request_id = PDO::lastInsertId(); 

		if ($tutor_id > 0) {
		      	$sql = 'INSERT INTO status_updates (`created`, `request_id`, `status_id`, `tutor_id`) VALUES (:created, :request_id, :status_id,  :tutor_id )';
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':created', $now);
			$stmt->bindValue(':request_id', $request_id);
			$stmt->bindValue(':status_id', $new_status);
			$stmt->bindValue(':tutor_id', $tutor_id);
		} else {
		      	$sql = 'INSERT INTO status_updates (`created`, `request_id`, `status_id`) VALUES (:created, :request_id, :status_id)';
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':created', $now);
			$stmt->bindValue(':request_id', $request_id);
			$stmt->bindValue(':status_id', $new_status);
		}		

		$status_inserted = $stmt->execute();
		if (!$status_inserted) {
			# report this error;
		}

	} else {
		$response = "error";
	}

	$pdo = null;

} catch(PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}


include("header.php");

echo $response;

echo '<meta http-equiv="refresh" content="0;url=lookup.php" />';

?>