<?php

function list_tutors($pdo) {
	$output = "";

	try {
		// $pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);
		// get a list of active requests from today (ignore anything created prior to today);
		$stmt = $pdo->prepare(' SELECT tutors.tutor_id, `first_name`, `last_name`, `email_address`, `date_added` FROM tutors WHERE status = 1 ');
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		// $row_count = $stmt->rowCount();

		if($rows !== false) {
			foreach($rows as $row) {
				$tutor_id = $row['tutor_id'];
				$date_added = $row['date_added'];
				$email = $row['email_address'];

				$output[] = "<tr><td><a href=schedule-tutor.php?tid=" . $tutor_id . ">" . $row['tutor_id'] . "</a></td><td>" . $row['first_name']. "</td><td> " . $row['last_name']. "</td><td> " . $email. "</td><td> " . $date_added . "</td> <td> ";

				# get the courses this tutor helps with
				$stmt2 = $pdo->prepare(' SELECT tutor_course.subject_crn, CONCAT(subject_crn.subject, " ", subject_crn.course) as subject_course FROM tutor_course JOIN subject_crn ON tutor_course.subject_crn = subject_crn.record_id WHERE tutor_course.tutor_id = :tutor_id ');
				$stmt2->bindValue(':tutor_id', $tutor_id);
				$stmt2->execute();
				$rows2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
				if($rows2 !== false) {
					$courses = [];
					foreach($rows2 as $row2) {
						$courses[] = $row2['subject_course'];
					}
					$courses_html = implode(", ", $courses);
					$output[] = $courses_html;
				}
				$output[] = "</td></tr>";
			}
		}

		$pdo = null;

		$response = $output;

	} catch(PDOException $e) {
	    	$response = 'ERROR: ' . $e->getMessage();
	}

	return $response;
}
