<?php

function get_tables_positions($pdo) {
	$output = "";

	try {
		// $pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);
		// get a list of active requests from today (ignore anything created prior to today);
		$stmt = $pdo->prepare('SELECT `table_number`, `table_position` FROM tables_positions ');
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		// $row_count = $stmt->rowCount();

		if($rows !== false) {
			foreach($rows as $row) {
				$output[] = $row['table_number'] . $row['table_position'];
			}
		}

		$pdo = null;

		$response = $output;

	} catch(PDOException $e) {
	    	$response = 'ERROR: ' . $e->getMessage();
	}

	return $response;
}
